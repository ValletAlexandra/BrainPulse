# Welcome to the MkDocs of Brain Pulse

Documentation of Brain Pulse code.


## Code structure

We provide an overview of the modules and class structure.

- **Config** - Reads and stores the problem configuration, including all options and settings from the input file (extension .yml).
- **Output** - Writes the output of the simulation in a user-specified format (Paraview).
- **Topology** - Reads and processes the input data describing the vessel network topology (node positions or edge lengths, connectivity)
- **Mesh** - Used to create a finite volume grid based on the topology in order to store variables in every cell of the grid, as well as parameters depending on the system of equations being solved. For example, the MeshBlood child class stores the variables and parameters for the blood flow equations, while the MeshBloodCSF child class will also store the variables and parameters for the PVS CSF flow. 
- **Solver** - This class discretizes the system of governing equations using the numerical schemes specified in the input file. There are several child classes that provide several (aimed to be extended) discretization techniques. 
- **Output** - This class provides several output writers that allow to save the simulation results. Several child classes provides several writer for specific output format (data along a vessel pathway, paraview)


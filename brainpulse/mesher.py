## todo: allow other discretization strategies, like number of cells by vessels instead of imposed dx.

from abc import ABCMeta, abstractmethod
import numpy as np


class Mesher(object):
    """ Context block of the strategy pattern for the mesher """

    def __init__(self, Solver, mesh=None, *args, **kwargs):

        self.dx = mesh['dx']
        self.simplyGeometry = mesh['simplify']

        self._strategy = parse_topology(listMeshers, Solver.mesher)
        # int, number of variables (nb of rows in U)
        self.size = Solver.nbVariables
        #store the solver needs
        self.parameters = Solver.parameters
        self.asignmethod = Solver.parameters_assignement
        self.nbVariables = Solver.nbVariables

    def mesh(self, *args, **kwargs):
        print('Creating the mesh with the %s method' % (self._strategy.name))
        print(self._strategy.description)
        mesh = self._strategy.mesh(self, *args, **kwargs)
        return mesh


class MesherStrategy(metaclass=ABCMeta):
    """Abstract base strategy class for the mesher"""

    def __init__(self, type):
        self._topologyreadertype = type

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def description(self):
        pass

    @abstractmethod
    def mesh(self, *args, **kwargs):
        """Build an object topology"""


class MesherNotKnown(MesherStrategy):
    name = "Unknown"
    description = "Unknown method"

    def mesh(self, *args, **kwargs):
        print("No Mesher named %s" % self._topologyreadertype)
        exit(1)


class NetworkMesher(MesherStrategy):
    name = "Network mesher"
    description = "Network mesher : for blood flow resolution with constant external pressure where each vessel is discretized with a finite volume method, each node represent either a BC or a junction."

    def mesh(self, Topology, *args, **kwargs):
        import brainpulse.mesh as mesh

        Network = mesh.Network()

        ## Preprocessing on the vessels topology
        # If asked, the geometry is simplified : consecutive edges are merged if not around a bifurcation
        # todo : should conserv the orientation
        if self.simplyGeometry == True:
            print('Simplification of the geometry : adjacent vessels are merged')
            Topology.simplify()
        # The Topology is oriented such as every junction has at least one parent vessel and one daughter vessel
        # Todo : Maybe this is not required for the linear scheme. In this case just remove this line
        Topology.set_orientation_Stronglyconnected()

        # Relabel the nodes with integer label with increasing order from 0 to N and store the label mapping
        # Todo : create the mapping like for the edges
        N = len(Topology)
        mappingnodes = dict(zip(Topology, range(0, N)))
        # storage of the mapping
        Network.mappingnodes = mappingnodes

        ## Creation of the mesh
        # For each edge we create a vessel with an integer label and we store the edge labels in a mapping
        mappingedges = {}
        for i, e in enumerate(Topology.edges):
            # creation of a vessel
            v = mesh.Vessel()
            v.setId(i)
            # set the topological data
            # coordinate of the points where to project the solution for visualisation
            v.setCoords(Topology.edges[e]['pos'])
            # radius along the vessel at s coordinate
            v.setRadius(Topology.edges[e]['radius'])
            # curvilinear s coordinate
            v.setCurvilinearCoordinate(Topology.edges[e]['s'])

            # meshing properties
            nbCells = max(int(Topology.edges[e]['length']/self.dx), 1)
            v.dx = float(Topology.edges[e]['length']/(nbCells))
            v.L = Topology.edges[e]['length']

            # Coordinates of the cells
            v.xim = np.array([i * v.dx for i in range(nbCells)],
                             np.float64)  # left face
            v.xip = np.array(
                [v.dx + i * v.dx for i in range(nbCells)], np.float64)  # right face
            v.xi = (v.xim + v.xip) / 2  # center of the cell

            # Ghost cells
            v.UghostL = np.zeros(self.nbVariables, np.float64)
            v.UghostR = np.zeros(self.nbVariables, np.float64)

            # Riemman invariant in ghost cells
            v.WghostL = np.zeros(self.nbVariables, np.float64)
            v.WghostR = np.zeros(self.nbVariables, np.float64)

            # Riemman invariant in ghost cells
            v.WghostLlast = np.zeros(self.nbVariables, np.float64)
            v.WghostRlast = np.zeros(self.nbVariables, np.float64)

            # Ghost cells
            v.FghostL = np.zeros(self.nbVariables, np.float64)
            v.FghostR = np.zeros(self.nbVariables, np.float64)

            #Normalized initial area
            v.Au = np.zeros(nbCells, np.float64)
            # Iterative solution in cells

            v.U = np.zeros([self.nbVariables, nbCells], np.float64)

            # Iterative solution at faces
            v.UL = np.zeros([self.nbVariables, nbCells+1], np.float64)
            v.UR = np.zeros([self.nbVariables, nbCells+1], np.float64)

            # Iterative fluxes at faces
            v.FL = np.zeros([self.nbVariables, nbCells+1], np.float64)
            v.FR = np.zeros([self.nbVariables, nbCells+1], np.float64)
            # Iterative Flux Arrays
            v.Flux = np.zeros([self.nbVariables, nbCells+1], np.float64)

            #Storage of the parameters
            ## todo : use a strategy pattern here
            properties = {}
            if self.asignmethod == 'constant':
                for prop in Topology.nodes[e[0]]:
                    properties[prop] = np.mean(Topology.edges[e][prop])
            else:
                print(
                    'ERROR, method of assignement of properties specified by the solver is not implemented')
                exit(1)

            for prop in self.parameters:
                v.Parameters[prop] = self.parameters[prop](properties, v.dx)

            Network.addVessel(v)
            mappingedges[e] = i

        # storage of the mapping
        Network.mappingedges = mappingedges

        #For each node of the graph we create a node object and add it to the network nodes
        for i, n in enumerate(Topology.nodes):
            Node = mesh.Node()
            Node.setId(i)
            for pn in Topology.predecessors(n):
                v = Network.vessels[mappingedges[(pn, n)]]
                Node.addParent(v)
            for sn in Topology.successors(n):
                v = Network.vessels[mappingedges[(n, sn)]]
                Node.addDaughter(v)
            Network.addNode(Node)

            #If it is a junction node we also add it to the inner nodes
            if Topology.degree[n] > 1:
                Network.addInnernode(Node)

        return Network


# To update when adding a new mesher
listMeshers = {"Network": NetworkMesher}


def parse_topology(listMeshers, type):
    strategy = listMeshers.setdefault(type, MesherNotKnown)
    return strategy


def meshertype(problem, solver):
    if (problem[0] == 'bloodflow') & (solver['type'] == 'linear'):
        return 'Network'
    else:
        print('No mesher implemented for this problem and solver combination')

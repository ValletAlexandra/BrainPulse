## I dont like the way the bc are implemented. I need a communication between the solver and the bc, but I am not sure about the way to do that.
# todo : add a default BC for BC nodes without BC conditions
# todo : error message if not a BC by BC node

import numpy as np
#import ast


class boundaryConditions(object):
    '''
    This class store the data concerning the boundary conditions (Imposed flow, pressure or others)

    This class provides the following methods:
    set_BC : this is a function taking as entry the solver. It will set the boundary condition depending on the solver and on the type of BC

    '''

    def __init__(self, mesh, type=None, function=None, value=None, nodes=[],  *args, **kwargs):
        '''
        Constructor
        '''
        self.type = type  # the type of boundary condition
        self._function = None  # function get_value providing the value of the imposed quantity
        self.parameters = []  # parameters used to compute the BC
        # list of vessels where the boundary condition must be applied on the left ghost cell
        self.vesselsin = []
        # list of vessels where the boundary condition must be applied on the right ghost cell
        self.vesselsout = []

        if nodes == []:
            print('Boundary condition creation for all the nodes ')
            for n in mesh.nodes:
                for v in n.Parents:
                    self.vesselsout.append(v)
                for v in n.Daughters:
                    self.vesselsin.append(v)
        else:
            # list of nodes where to impose the bc
            nodes = [n for n in map(int, str(nodes).split(','))]
            print('Boundary condition creation for the nodes :', nodes)

            for i in nodes:
                n = mesh.nodes[mesh.mappingnodes[i]]
                for v in n.Parents:
                    self.vesselsout.append(v)
                for v in n.Daughters:
                    self.vesselsin.append(v)

        if value is not None:
            # the parameters provided to define the function
            self.parameters = [n for n in map(float, str(value).split(','))]

        ## The strategy  to impose the BC
        if type == 'flow':
            print('The flow is imposed')
            self._set_BC = self.setBCFlow
        elif type == 'pressure':
            print('The pressure is imposed')
            self._set_BC = self.setBCPressure
        elif type == 'noreflection':
            print('no reflection is imposed')
            self._set_BC = self.setBCNoReflexion
        elif type == 'windkessel':
            print('a windkessel 0D model is imposed')
            self._set_BC = self.setBCWindkessel
        elif type == 'nrwindkessel':
            print('a windkessel 0D model without reflection is imposed')
            self._set_BC = self.setBCNRWindkessel
        elif type == 'resistance':
            print('a resistance 0D model is imposed')
            self._set_BC = self.setBCResistance
        else:
            print(
                'Error, the boundary condition %s is not implemented for this solver' % type)
            exit(1)

        if function:
            if not self.parameters:
                print(
                    'ERROR : the parameters for the boundary condition function is missing')
                exit(1)

            if function == "constant":
                print('with a constant value.')
                self._function = lambda t, k=self.parameters[0]: k
            elif function == "exponential":
                print('with an exponential time dependent value.')
                self._function = lambda t, a=self.parameters[0], b=self.parameters[1], c=self.parameters[2]: a*(
                    np.exp(-b*t)-c)
            elif function == "linear":
                print('with a linear time dependent value.')
                self._function = lambda t, a=self.parameters[0], b=self.parameters[1]: a*t+b
            elif function == "sinus":
                print('with a sinus time dependent value.')
                self._function = lambda t, amp=self.parameters[0], T=self.parameters[1]: amp*np.sin(
                    2*np.pi*t/T)
            elif function == "triangle":
                print('with a triangular function time dependent value.')
                self._function = lambda t, amp=self.parameters[0], minval=self.parameters[1], T=self.parameters[2]/2, nmax=self.parameters[3], m=self.parameters[4]: (-2*m**2/(
                    (m-1)*np.pi**2)*sum([(-1)**n/(n**2)*np.sin(n*(m-1)*np.pi/m)*np.sin(n*np.pi*t/T) for n in range(1, int(nmax))])+1)/2*amp+minval
            elif function == "pulse":
                print('with a pulse time dependent value.')
                self._function = lambda t, a=self.parameters[0], T=self.parameters[2], tau=self.parameters[1]: a*(
                    np.sin(np.pi*(t % T)/tau))*np.heaviside(tau-t % T, 0)
            elif function == "onepulse":
                print('with a one pulse time dependent value.')
                self._function = lambda t, a=self.parameters[0], tau=self.parameters[1]: a*(
                    np.sin(np.pi*(t)/tau))*np.heaviside(tau-t, 0)
            else:
                print('ERROR: the function for he bc is not implemented')
                exit(1)

    def set_BC(self, solver, dt, t):
        self._set_BC(solver, dt, t)

    # comment : I don't realy like the fact that  if a new BC is implemented in the solver, it has to be added here as well. Is there a way to modify only one class when adding a bc ?

    def setBCResistance(self, solver, dt, t):
        for v in self.vesselsin:
            solver.setResistanceIN(v, self.parameters)
        for v in self.vesselsout:
            solver.setResistanceOUT(v, self.parameters)

    def setBCWindkessel(self, solver, dt, t):
        for v in self.vesselsin:
            solver.setWindkesselIN(v, self.parameters, dt)
        for v in self.vesselsout:
            solver.setWindkesselOUT(v, self.parameters, dt)

    def setBCNRWindkessel(self, solver, dt, t):
        for v in self.vesselsin:
            solver.setNRWindkesselIN(v, self.parameters, dt)
        for v in self.vesselsout:
            solver.setNRWindkesselOUT(v, self.parameters, dt)

    def setBCFlow(self, solver, dt, t):
        for v in self.vesselsin:
            solver.setFlowIN(v, self._function, dt, t)
        for v in self.vesselsout:
            solver.setFlowOUT(v, self._function, dt, t)

    def setBCPressure(self, solver, dt, t):
        for v in self.vesselsin:
            solver.setPressureIN(v, self._function, dt, t)
        for v in self.vesselsout:
            solver.setPressureOUT(v, self._function, dt, t)

    def setBCNoReflexion(self, solver, dt, t):
        for v in self.vesselsin:
            solver.setNoReflectionIN(v, dt)
        for v in self.vesselsout:
            solver.setNoReflectionOUT(v, dt)

    def add_vesselIN(self, v):
        self.vesselsin.append(v)

    def add_vesselOUT(self, v):
        self.vesselsout.append(v)

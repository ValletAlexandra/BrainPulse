from brainpulse.solver import AbsSolver
import brainpulse.parameters as parameters
import brainpulse.outputvariables as outputvariables
import numpy as np


class BFLinearSolver(AbsSolver):
    name = 'BFLinearSolver'
    description = 'Linear finite volume Solver for blood flow'
    mesher = 'Network'
    parameters = {'A0': parameters.funA0,
                  'c0': parameters.func0,
                  'coeffC': parameters.funcoefC,
                  'coeffR': parameters.funcoefR,
                  'coeffL': parameters.funcoefL,
                  'alpha': parameters.funAlpha,
                  'Z': parameters.funZ,
                  'Ci': parameters.funCi}
    variables = {'P': outputvariables.P,
                 'Q': outputvariables.Q,
                 'A': outputvariables.A,
                 'R': outputvariables.R,
                 }
    parameters_assignement = 'constant'
    nbVariables = 2

    def __init__(self, reconstruction_method=None, source_method='implicit', flux_method='LaxFriedrich', *args, **kwargs):

        print('Creation of a linear solver for blood flow')

        # Set the numerical flux computation method
        if flux_method == 'simple':
            print('Solver with simple numerical flux')
            self._ComputeFlux = FluxSimple
        elif flux_method == 'LaxFriedrich':
            print('Solver with LaxFriedrich numerical flux')
            self._ComputeFlux = FluxLaxFriedrich
        elif flux_method == 'LaxWendroff':
            print('Solver with LaxWendroff numerical flux')
            self._ComputeFlux = FluxLaxWendroff
        elif flux_method == 'Godunov':
            print('Solver with Godunov numerical flux')
            self._ComputeFlux = FluxGodunov
        elif flux_method == 'Richtmeyer':
            print('Solver with Richtmeyer numerical flux')
            self._ComputeFlux = FluxRichtmeyer
        else:
            print('ERROR : The numerical flux specified is not known')
            exit(1)

        # Set the reconstruction method
        if reconstruction_method == 'superbee':
            print('Solver with suberbee reconstruction')
            self._ComputeReconstruction = ReconstructionSuperBee
        elif reconstruction_method == 'minmod':
            print('Solver with minmod reconstruction')
            self._ComputeReconstruction = ReconstructionMinMod
        else:
            print('Solver with no slope reconstruction')
            self._ComputeReconstruction = ReconstructionNoslope

        if source_method == 'implicit':
            self._computesource = implicitSource
        else:
            print(
                'ERROR : the numerical method specified for the source term computation is not knowm')
            exit(1)

    def computedt(self, mesh, CFL):
        dt = 1.0
        for v in mesh.vessels:
            vessel_dt = CFL * v.dx / v.Parameters['c0'] / 2
            dt = min(dt, vessel_dt)
        return dt

    def init(self, mesh):
        print('Initialization of the mesh')
        mesh.t = 0
        for v in mesh.vessels:
            v.Au = np.interp(v.xi, v.s, np.pi * v.radius0 **
                             2) / v.Parameters['A0']
            # IMPORTANT :  for now, only constant radius along a vessel is considered, because the BC and source term coherency with varying Au has to be checked
            v.Au = np.ones_like(v.Au)

        # todo : allow other type of initialisation for test cases

    def advancetime(self, mesh, boundaryconditions, dt, t):

        # From state at time tn, update the characteristics in the ghost cells
        UpdateCharacteristics(mesh, dt)

        #todo : question, is it okay from the to provide the solver itself here in the bc function in order to use the solver function

        # Solve internal nodes, hyperbolic part
        self.solveHyperbolic(mesh, dt, t)

        # Solve internal nodes, source term part
        self.solveSource(mesh, dt)

        # Update ghost cells for the next time step
        # Boundary conditions
        for bc in boundaryconditions:
            bc.set_BC(self, dt, t)

        # Junction nodes
        for n in mesh.innernodes:
            setjunction(n, dt, t)

        mesh.t += dt

    def setFlowIN(self, V, function, dt, t):
        ### Valeur moyenne du débit sur l'interface entre tn+1 et tn+2 avec correction pour le terme source
        # Hypothèse linéaire, la valeur moyenne est la valeur en (tn+1+tn+2)/2
        Qbar = function(t + 1.5 * dt) * (1 +
                                         V.Parameters['coeffR'] * V.dx / 2 / V.Parameters['Z'])
        # Update of the ingoing characterisics at the time tn+1 using the inlet function
        V.WghostL[1] = Qbar * V.Parameters['Z'] + \
            riemannInvariants(V.U[:, 0], V.Parameters)[0]

        # Calculate U from the characterisitcs
        ## Todo creer une fonction pour ca
        V.UghostL[0] = V.WghostL[0] + V.WghostL[1]
        V.UghostL[1] = (V.WghostL[1] - V.WghostL[0]) / V.Parameters['Z']

    def setFlowOUT(self, V, function, dt, t):
        ### Valeur moyenne du débit sur l'interface entre tn+1 et tn+2 avec correction pour le terme source
        # Hypothèse linéaire, la valeur moyenne est la valeur en (tn+1+tn+2)/2
        Qbar = -function(t + 1.5 * dt) * (1 +
                                          V.Parameters['coeffR'] * V.dx / 2 / V.Parameters['Z'])

        # Update of the ingoing characterisics at the time tn+1 using the inlet function
        V.WghostR[0] = -Qbar * V.Parameters['Z'] + \
            riemannInvariants(V.U[:, -1], V.Parameters)[1]

        # Calculate U from the characterisitcs
        ## Todo creer une fonction pour ca
        V.UghostR[0] = V.WghostR[0] + V.WghostR[1]
        V.UghostR[1] = (V.WghostR[1] - V.WghostR[0]) / V.Parameters['Z']

    def setPressureIN(self, V, function, dt, t):
        ### Valeur moyenne de la pression sur l'interface entre tn+1 et tn+2
        # Hypothèse linéaire, la valeur moyenne est la valeur en (tn+1+tn+2)/2
        Pbar = function(t + 1.5 * dt)

        # Update of the ingoing charachterisics at the time tn+1 using the inlet function
        V.WghostL[1] = Pbar - riemannInvariants(V.U[:, 0], V.Parameters)[0]

        # Calculate U from the characterisitcs
        # To do : create a function for this
        V.UghostL[0] = V.WghostL[0] + V.WghostL[1]
        V.UghostL[1] = (V.WghostL[1] - V.WghostL[0]) / V.Parameters['Z']

    def setPressureOUT(self, V, function, dt, t):
        ### Valeur moyenne de la pression sur l'interface entre tn+1 et tn+2
        # Hypothèse linéaire, la valeur moyenne est la valeur en (tn+1+tn+2)/2
        Pbar = function(t + 1.5 * dt)

        # Update of the ingoing (left) W1 charachterisics at the time tn+1 using the inlet function
        V.WghostR[0] = Pbar - riemannInvariants(V.U[:, -1], V.Parameters)[1]

        # Calculate U from the characterisitcs
        # To do : create a function for this
        V.UghostR[0] = V.WghostR[0] + V.WghostR[1]
        V.UghostR[1] = (V.WghostR[1] - V.WghostR[0]) / V.Parameters['Z']

    def setNoReflectionIN(self, V, dt):

        V.WghostL[1] = 0

        # Calculate U from the characterisitcs
        # To do : create a function for this
        V.UghostL[0] = V.WghostL[0] + V.WghostL[1]
        V.UghostL[1] = (V.WghostL[1] - V.WghostL[0]) / V.Parameters['Z']

    def setNoReflectionOUT(self, V, dt):

        V.WghostR[0] = 0
        # Calculate U from the characterisitcs
        # To do : create a function for this
        V.UghostR[0] = V.WghostR[0] + V.WghostR[1]
        V.UghostR[1] = (V.WghostR[1] - V.WghostR[0]) / V.Parameters['Z']

    def setResistanceIN(self, V, parameters):
        # Update of the ingoing (right)  characteristics at the time tn+1

        R, Pout = parameters[0:2]

        V.WghostL[1] = (Pout/R - riemannInvariants(V.U[:, 0], V.Parameters)
                        [0]*(1/R-1/V.Parameters['Z']))/(1/R+1/V.Parameters['Z'])

        # Calculate U from the characterisitcs
        # To do : create a function for this
        V.UghostL[0] = V.WghostL[0] + V.WghostL[1]
        V.UghostL[1] = (V.WghostL[1] - V.WghostL[0]) / V.Parameters['Z']

    def setResistanceOUT(self, V, parameters):
        # Update of the ingoing (right)  charachterisics at the time tn+1

        R, Pout = parameters[0:2]

        V.WghostR[0] = (Pout/R - riemannInvariants(V.U[:, -1], V.Parameters)
                        [1]*(1/R-1/V.Parameters['Z']))/(1/R+1/V.Parameters['Z'])

        # Calculate U from the characterisitcs
        # To do : create a function for this
        V.UghostR[0] = V.WghostR[0] + V.WghostR[1]
        V.UghostR[1] = (V.WghostR[1] - V.WghostR[0]) / V.Parameters['Z']

    def setWindkesselIN(self, V, parameters, dt):
        R1, R2, C, Pout = parameters[0:4]

        K0 = 1 / V.Parameters['Z']

        DeltaW1 = V.WghostL[0]-V.WghostLlast[0]

        DeltaW2 = (-V.UghostR[1] * (R1 + R2) - V.UghostL[0] - Pout + DeltaW1 * (K0*(R1+R2)
                                                                                + C * R2 / dt * (R1 * K0 - 1)-1)) / (K0*(R1+R2) + C * R2 / dt * (R1*K0 + 1) + 1)

        V.WghostL[1] += DeltaW2

        V.UghostL[0] = V.WghostL[0] + V.WghostL[1]
        V.UghostL[1] = (V.WghostL[1] - V.WghostL[0]) / V.Parameters['Z']

    def setWindkesselOUT(self, V, parameters, dt):
        R1, R2, C, Pout = parameters[0:4]

        K0 = 1 / V.Parameters['Z']

        DeltaW2 = V.WghostR[1]-V.WghostRlast[1]

        DeltaW1 = (V.UghostR[1] * (R1 + R2) - V.UghostR[0] + Pout + DeltaW2 * (K0*(R1+R2)
                                                                               + C * R2 / dt * (R1 * K0 - 1)-1)) / (K0*(R1+R2) + C * R2 / dt * (R1*K0 + 1) + 1)

        V.WghostR[0] += DeltaW1

        V.UghostR[0] = V.WghostR[0] + V.WghostR[1]
        V.UghostR[1] = (V.WghostR[1] - V.WghostR[0]) / V.Parameters['Z']

    def setNRWindkesselIN(self, V, parameters, dt):
        R1, R2, C, Pout = parameters[0:4]

        K0 = 1 / V.Parameters['Z']

        #a calculer au depart
        if R2 <= 1/K0:
           R1 = 1/K0-R2
        else:
           R1 = 1/K0/2
           R2 = 1/K0/2

        DeltaZ = (V.WghostL[1] - riemannInvariants(V.U[:, 0],
                  V.Parameters)[1]) * V.Parameters['c0'] * dt / V.dx * 2
        # (-(V.UghostL[1] * (R1 + R2) - V.UghostL[0] + Pout) + DeltaZ * (
        DeltaW = 0
        # K0 * (R1 + R2) + C * R2 / dt * (R1*K0 + 1) + 1)) / (K0 * (R1 + R2) + C * R2 / dt * (R1 * K0 - 1 ) - 1)

        V.UghostL[0] += DeltaZ + DeltaW
        V.UghostL[1] += (DeltaW - DeltaZ) / (V.Parameters['Z'])

    def setNRWindkesselOUT(self, V, parameters, dt):
        R1, R2, C, Pout = parameters[0:4]

        K0 = 1 / V.Parameters['Z']

        #a calculer au depart
        if R2 <= 1/K0:
            R1 = 1/K0-R2
        else:
            R1 = 1/K0/2
            R2 = 1/K0/2

        DeltaW = -(V.WghostR[0] - riemannInvariants(V.U[:, -1],
                   V.Parameters)[0]) * V.Parameters['c0'] * dt / V.dx * 2

        DeltaZ = (V.UghostR[1] * (R1 + R2) - V.UghostR[0] + Pout + DeltaW * (
            K0 * (R1 + R2) + C * R2 / dt * (R1 * K0 - 1) - 1)) / (K0 * (R1 + R2) + C * R2 / dt * (R1*K0 + 1) + 1)

        V.UghostR[0] += DeltaZ + DeltaW
        V.UghostR[1] += K0 * (DeltaW - DeltaZ)

    # self is useless here, should I put this function out of the class ?

    def solveHyperbolic(self, mesh, dt, t):
        for V in mesh.vessels:
            # MUSCL reconstruction
            self._ComputeReconstruction(V)

            # Numerical flux
            # 'LaxFriedrich''LaxWendroff' 'Godunov''Richtmeyer'
            self._ComputeFlux(V, dt)

            # Cumpute time derivative
            dUdt = (V.Flux[:, 1:] - V.Flux[:, :-1]) / V.dx

            # Advance in time
            V.U = V.U - dt * dUdt

    # self is useless here, should I put this function out of the class ?
    def solveSource(self, mesh, dt):
        # Implicit method
        for V in mesh.vessels:
            self._computesource(V, dt)


def UpdateCharacteristics(mesh, dt):

    for V in mesh.vessels:
        V.WghostLlast[0] = V.WghostL[0]
        V.WghostRlast[1] = V.WghostR[1]

        ## Update of the left ghost cell outgoing characteristic, by propagating the left-going characerisitcs W1 in first cell
        V.WghostL[0] = V.WghostL[0] + V.Parameters['c0'] * dt / V.dx * (
            riemannInvariants(V.U[:, 0], V.Parameters)[0] - V.WghostL[0])
        ## Update of the right ghost cell outgoing characteristic, by propagating the right-going characerisitcs W2 in last cell
        V.WghostR[1] = V.WghostR[1] + V.Parameters['c0'] * dt / V.dx * (
            riemannInvariants(V.U[:, -1], V.Parameters)[1] - V.WghostR[1])


def implicitSource(V, dt):
    V.U[1, :] = V.U[1, :] / (1 + V.Parameters['alpha']
                             * dt)  # /need to check about Au


def setjunction(n, dt, t):
    V0 = n.Parents[0]
    W2N0 = riemannInvariants(V0.U[:, -1], V0.Parameters)[1]
    V0.WghostR[0] = sum([(2 * riemannInvariants(Vp.U[:, -1], Vp.Parameters)[1] - W2N0) * Vp.Parameters['Ci'] for Vp in
                         n.Parents])
    V0.WghostR[0] += sum(
        [(2 * riemannInvariants(Vd.U[:, 0], Vd.Parameters)[0] - W2N0) * Vd.Parameters['Ci'] for Vd in
         n.Daughters])
    V0.WghostR[0] /= sum([V.Parameters['Ci'] for V in n.Parents + n.Daughters])
    V0.UghostR[0] = V0.WghostR[0] + V0.WghostR[1]
    V0.UghostR[1] = (V0.WghostR[1] - V0.WghostR[0]) / V0.Parameters['Z']

    for Vp in n.Parents[1::]:
        Vp.WghostR[0] = V0.WghostR[0] + W2N0 - \
            riemannInvariants(Vp.U[:, -1], Vp.Parameters)[1]
        Vp.UghostR[0] = Vp.WghostR[0] + Vp.WghostR[1]
        Vp.UghostR[1] = (Vp.WghostR[1] - Vp.WghostR[0]) / Vp.Parameters['Z']

    for Vd in n.Daughters:
        Vd.WghostL[1] = V0.WghostR[0] + W2N0 - \
            riemannInvariants(Vd.U[:, 0], Vd.Parameters)[0]
        Vd.UghostL[0] = Vd.WghostL[0] + Vd.WghostL[1]
        Vd.UghostL[1] = (Vd.WghostL[1] - Vd.WghostL[0]) / Vd.Parameters['Z']


def riemannInvariants(U, Parameters):
    W1 = (U[0] - Parameters['coeffL'] * Parameters['c0'] * U[1]) / 2
    W2 = (U[0] + Parameters['coeffL'] * Parameters['c0'] * U[1]) / 2
    return W1, W2


def updateULUR(V):
    V.UL[0, :] = np.concatenate([[V.UghostL[0]], V.U[0, :]])
    V.UL[1, :] = np.concatenate([[V.UghostL[1]], V.U[1, :]])

    V.UR[0, :] = np.concatenate([V.U[0, :], [V.UghostR[0]]])
    V.UR[1, :] = np.concatenate([V.U[1, :], [V.UghostR[1]]])


def updateFLFR(V):
    V.FL[0, :] = V.UL[1, :] / V.Parameters['coeffC']
    V.FL[1, :] = V.UL[0, :] / V.Parameters['coeffL']

    V.FR[0, :] = V.UR[1, :] / V.Parameters['coeffC']
    V.FR[1, :] = V.UR[0, :] / V.Parameters['coeffL']


def ReconstructionNoslope(V):
    updateULUR(V)
    updateFLFR(V)


def ReconstructionSuperBee(V, method='noslope'):
    updateULUR(V)

    Dp = (V.UR[:, 1:] - V.UR[:, :-1]) / V.dx
    Dm = (V.UL[:, 1:] - V.UL[:, :-1]) / V.dx
    #    Dc=(V.UR[:,1:]-V.UL[:,:-1])/2/V.dx

    Slope = superbeeSlope(Dm, Dp)

    V.UR[:, :-1] = V.UR[:, :-1] - Slope * V.dx / 2
    V.UL[:, 1:] = V.UL[:, 1:] + Slope * V.dx / 2

    updateFLFR(V)


def ReconstructionMinMod(V):
    updateULUR(V)

    Dp = (V.UR[:, 1:] - V.UR[:, :-1]) / V.dx
    Dm = (V.UL[:, 1:] - V.UL[:, :-1]) / V.dx
    #    Dc=(V.UR[:,1:]-V.UL[:,:-1])/2/V.dx

    Slope = minmodSlope(Dm, Dp)

    V.UR[:, :-1] = V.UR[:, :-1] - Slope * V.dx / 2
    V.UL[:, 1:] = V.UL[:, 1:] + Slope * V.dx / 2

    updateFLFR(V)


def FluxSimple(V, dt):
    # Simplecentered flux (unstable)
    V.Flux = 0.5 * (V.FR + V.FL)


def FluxLaxFriedrich(V, dt):
    # V.Flux=0.5*(V.FR + V.FL -  (V.dx/dt)*(V.UR - V.UL))
    V.Flux = 0.5 * (V.FR + V.FL - V.Parameters['c0'] * (V.UR - V.UL))


def FluxRichtmeyer(V, dt):
    Uintermediaire = 0.5 * (V.UL + V.UR) - dt / V.dx / 2 * (V.FR - V.FL)
    V.Flux = flux(Uintermediaire, V.Parameters)


def FluxGodunov(V, dt):
    V.Flux[0, :] = 0.5 * (
        V.Parameters['c0'] * V.UL[0, :] + V.UL[1, :] / V.Parameters['coeffC'] - V.Parameters['c0'] * V.UR[0, :] + V.UR[1, :] / V.Parameters['coeffC'])
    V.Flux[1, :] = 0.5 * (
        V.UL[0, :] / V.Parameters['coeffL'] + V.Parameters['c0'] * V.UL[1, :] + V.UR[0, :] / V.Parameters['coeffL'] - V.Parameters['c0'] * V.UR[1, :])


def FluxLaxWendroff(V, dt):
    V.Flux[0, :] = 0.5 * (V.UR[1, :] + V.UL[1, :]) / V.Parameters['coeffC'] - 0.5 / V.dx * dt / V.Parameters[
        'coeffC'] / V.Parameters['coeffL'] * (V.UR[0, :] - V.UL[0, :])
    V.Flux[1, :] = 0.5 * (V.UR[0, :] + V.UL[0, :]) / V.Parameters['coeffL'] - 0.5 / V.dx * dt / V.Parameters[
        'coeffC'] / V.Parameters['coeffL'] * (V.UR[1, :] - V.UL[1, :])


def superbeeSlope(Dm, Dp):
    s1 = minmod(Dp, 2 * Dm)
    s2 = minmod(2 * Dp, Dm)
    return maxmod(s1, s2)


def minmodSlope(Dm, Dp):
    return minmod(Dm, Dp)


def maxmod(a, b):
    return 0.5 * (np.sign(a) + np.sign(b)) * np.maximum(np.abs(a), np.abs(b))


def minmod(a, b):
    return 0.5 * (np.sign(a) + np.sign(b)) * np.minimum(np.abs(a), np.abs(b))


def flux(U, Parameters):
    F = np.zeros_like(U)
    F[0, :] = U[1, :] / Parameters['coeffC']
    F[1, :] = U[0, :] / Parameters['coeffL']
    return F

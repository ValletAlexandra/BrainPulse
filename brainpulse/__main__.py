# -*- coding: utf-8 -*-

def main():
    from brainpulse.config import ReadConfig
    from brainpulse.topologyMaker import buildTopology
    from brainpulse.mesher import Mesher
    from brainpulse.BoundaryCondition import boundaryConditions
    from brainpulse.solver import Solver, SolverContext
    from brainpulse.output import outputs

    import logging

    # create logger
    logger = logging.getLogger('main')
    logger.setLevel(logging.DEBUG)

    # Todo use a parser to get the verbose degree
    # create console handler and set level to debug
    consolehandler = logging.StreamHandler()
    consolehandler.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to ch
    consolehandler.setFormatter(formatter)

    # add consolehandler to logger
    logger.addHandler(consolehandler)

    ## Todo use a parser for the input file
    #Define the inputfile location
#    configfile='data/example_config.yml'
#    configfile='data/WillisCircle.yml'
    configfile = 'data/WillisCalgary.yml'
    #configfile='data/Pulse-oneartery.yml'

    # Read the inputfile
    config = ReadConfig(configfile, logger)

    # Build the topology
    Topology = buildTopology(**config)

    # Create solver
    Solver = Solver(**config)

    # Create the mesh
    Mesher = Mesher(Solver, **config)
    Mesh = Mesher.mesh(Topology)

    # Create the list of boundary conditions
    boundary_conditions = [boundaryConditions(
        Mesh, **bc) for bc in config['vessels']['boundary_conditions']]

    #Create ouput
    Outputs = outputs(Mesh, Topology, Solver, **config)

    # Create solver object and run simulation
    Simulation = SolverContext(
        Mesh, Solver, boundary_conditions, Outputs, **config)
    Simulation.run()


if __name__ == "__main__":
    main()

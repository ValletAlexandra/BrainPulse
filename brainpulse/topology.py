# -*- coding: utf-8 -*-

#todo : prevoir les coordonnées internes comme attribut des edges.

#import utils
import networkx
import numpy as np
import matplotlib.pyplot as plt
from networkx.drawing.nx_pydot import graphviz_layout
from brainpulse.dataReader import DataReader


class Topology(networkx.DiGraph):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.edgesID = {}

    def add_node(self, node, coord):
        ##todo message erreur si noeud existe deja
        super().add_node(node, pos=coord)

    def add_edge(self, N1, N2, id=None, **kwargs):
        # todo message si edge existe deja
        if id is None:
            id = (N1, N2)

        super().add_edge(N1, N2, id=id, **kwargs)

        #todo : message erreur si id existe deja
        self.edgesID[id] = (N1, N2)

    def reverse_edge(self, e):
        attrs = self[e[0]][e[1]]
        self.remove_edge(*e)
        self.add_edge(e[1], e[0], **attrs)
        self.edgesID = {attrs['id']: (e[1], e[0])}
        # reverse also the order or the properties of the inner nodes
        for prop in self.nodes[e[0]]:
            self.edges[(e[1], e[0])][prop].reverse()

    def reverse_path(self, path):
        for e in zip(path[:-1], path[1:]):
            self.reverse_edge(e)

    def simplify(self):
        # All nodes of degree 2 will be suppressed. It will remain only boundary nodes and junction nodes. The equivalent edge lenght is set as an edge attribute.

        #In order to find path that correspond to vessels, the graph must be undirected.
        # I make two copy of the initial graph here, maybe its not ideal
        G = self.to_undirected()
        #Create a graph with only the innernodes
        H = G.copy()
        H.remove_nodes_from((n for n in G.nodes() if G.degree(n) != 2))

        if H:
            #Each connected component in this graph corresponds to one vessel
            for ivessel, list_nodes in enumerate(networkx.connected_components(H)):
                #Get the adjacent nodes in order to have the full vessel
                BoundaryNodes = list(networkx.node_boundary(G, list_nodes))
                vessel = G.subgraph(list(list_nodes)+BoundaryNodes)
                vessel_path = networkx.shortest_path(
                    vessel, BoundaryNodes[0], BoundaryNodes[1])
                ### get edges properties along the path, for storage
                properties = {}  # dict to store the edges properties
                for prop in G.nodes[BoundaryNodes[0]]:
                    properties[prop] = [G.nodes[BoundaryNodes[0]][prop]]
                length = 0
                properties['s'] = [0]  # curvilinear coordinate
                for i in range(1, len(vessel_path)):
                    for prop in G.nodes[BoundaryNodes[0]]:
                        properties[prop].append(G.nodes[vessel_path[i]][prop])
                    length += G.edges[(vessel_path[i-1],
                                       vessel_path[i])]['length']
                    properties['s'].append(length)

                self.remove_nodes_from(list_nodes)
                self.add_edge(
                    BoundaryNodes[0], BoundaryNodes[1], id=ivessel, length=length)
                for prop in properties:
                    self.edges[BoundaryNodes[0], BoundaryNodes[1]
                               ][prop] = properties[prop]

        else:
            print('This topology cannot be further simplified')

    def set_properties(self, properties):
        self.set_nodes_properties(properties)
        self.set_edges_properties()

    def set_nodes_properties(self, properties):
        for prop in properties:
            Reader = DataReader(**properties[prop])
            data = Reader.get_data(**properties[prop])
            #Checking coherency in the number of coordinates provided
            if len(data) == 1:
                networkx.set_node_attributes(self, data[0], prop)
            else:
                if len(data) != len(self.nodes):
                    print(
                        'The number of property values provided do not corresponds to the number of nodes')
                    exit(1)
                for n in self.nodes:
                    self.nodes[n][prop] = data[n]

    def set_edges_properties(self):
        for e in self.edges:
            for prop in self.nodes[e[0]]:
                self.edges[e][prop] = [self.nodes[e[0]]
                                       [prop], self.nodes[e[1]][prop]]
            self.edges[e]['s'] = [0, self.edges[e]['length']]

    def check_compatibility_flowsolver(self):

        if [k for k, v in dict(self.degree()).items() if v == 0]:
            print("There are isolated nodes in the graph")
            return False  # ou ici supprimer les noeuds et le mettre dans le log
        elif not [k for k, v in dict(self.degree()).items() if v == 1]:
            print("There are no boundary nodes")
            return False
        elif [e for e in self.edges if (e[1], e[0]) in self.edges]:
            print("There are non-oriented edges")
            return False
        else:
            no_indegrees = [k for k, v in dict(
                self.in_degree()).items() if v == 0]
            no_outdegrees = [k for k, v in dict(
                self.out_degree()).items() if v == 0]

            rep = True
            for v in no_indegrees + no_outdegrees:
                if self.degree[v] > 1:
                    print("We didn't find a valid orientation")
                    rep = False
            if rep:
                print(
                    "Well done ! This topology is compatible with a blood flow simulation")
            else:
                print(
                    "Oh no ! This topology is not compatible with a blood flow simulation.")

            return rep

    def set_orientation_Random(self):
        edges_list = list(self.edges)

        while edges_list:
            e = edges_list.pop()
            i = np.random.randint(0, 2)  # equals 0 or 1
            # If this edge is bi-directed we remove the other
            if self.has_edge(e[1], e[0]):
                self.remove_edge(e[1], e[0])
                edges_list.remove((e[1], e[0]))
            if i:  # we randomly reverse the edge
                self.reverse_edge(e)

    def set_orientation_Stronglyconnected(self, root=None):
        """One can find a strongly-connected orientation in linear time using depth-first search:
        
        orient all edges in the depth-first search tree downward awayfrom the root and
        orient all the non-tree edges upward with respect to thetree, 
        cross edges may be oriented arbitrarily."""

        # Convert the graph in a bidirected graph
        H = self.to_undirected()
        H = H.to_directed()

        networkx.set_edge_attributes(H, False, 'visited')

        if root is None:
            # set the root
            # list of nodes with degree=1, ie boundary nodes (on prend v=2 car bi-directed)
            # select one as the root
            boundarynodes = [k for k, v in dict(H.degree()).items() if v == 2]
            root = boundarynodes[0]

            # Returns oriented tree constructed from a depth-first-search from root
        DFStree = networkx.dfs_tree(H, source=root)
        for e in DFStree.edges:
            if e not in self.edges:
                self.reverse_edge((e[1], e[0]))
            H.remove_edge(e[1], e[0])
            H.edges[e]['visited'] = True
            # self.edges[e]['color']='r'

        starting_nodes = [k for k, v in dict(
            DFStree.degree()).items() if v == 1]

        ## On peut modifier sans avoir un bidirected,
        ##il faut creer un bidirected
        ##et pour chaque edge du graph, si il est pas dans le graph orienté alors reverse.

        while starting_nodes:
            for sn in starting_nodes:
                voisins = [n for n in H.successors(
                    sn) if H[sn][n]['visited'] == False]
                for n in voisins:
                    H[sn][n]['visited'] = True
                    H.remove_edge(n, sn)
                    if (n, sn) in self.edges:
                        self.reverse_edge((n, sn))
                    # self.edges[(sn,n)]['color']='g'
                starting_nodes = voisins

        edgeslist = [e for e in H.edges if (e[1], e[0]) in H.edges]
        # for e in edgeslist :
        # if e in self.edges :
        # self.edges[e]['color']='b'

        while edgeslist:
            e = edgeslist.pop()
            edgeslist.remove((e[1], e[0]))
            H.remove_edge(e[1], e[0])

    def set_orientation_Pathreversal(self):
        """ 
        Graph orientation algorithm first given bt Fraysseix and de Mendez that find
        an orientation of an undirected graph that minimizes the maximum indegree.
        
        This is not sufficient for out application as the outdegree can be 0.
        
        Path-Reversal algorithm : 
            1/arbitrarily orient every edge 
    
            2/while there is a reversible path 
                let P be any reversible path whose last vertex is of highest indegree reverse the orientation 
                of each arc of P
    
        A directed path from u to v is reversible if indegree(u)< indegree(v)−1
        This algorithm can be perfomed in a quadractic time.
        
        A directed path from vertex u to v is reversible if indegree(u)<indegree(v)-1 """

        # Generate random orientation #If the graph is not bidirected, its unnecessary
        self.set_orientation_Random()

        ExistsReversiblePath = True

        # set target_indegree to the maximum indegree value
        target_indegree = max(dict(self.in_degree()).values())

        while ExistsReversiblePath:
            # All nodes with target_indegree indegrees
            targets = [k for k, v in dict(
                self.in_degree()).items() if v == target_indegree]
            # All nodes that could lead to a revesible path if a path exists
            sources = [k for k, v in dict(
                self.in_degree()).items() if v < target_indegree - 1]

            if not sources:
                # If no such nodes exists then there is no reversible paths in the graph
                ExistsReversiblePath = False
            else:
                # Looking for possible path between sources and target.
                # If they exists they are reversibles.
                j = 0
                P = []
                while (not P) & (j < len(targets)):
                    i = 0
                    while (not P) & (i < len(sources)):
                        P = list(networkx.all_simple_paths(
                            self, source=sources[i], target=targets[j]))
                        i += 1
                    j += 1

                if not P:
                    # If no reversible path exists with the current target_indegree,
                    # we try again with a lower larger indegree if possible
                    if target_indegree > 2:
                        target_indegree -= 1
                    else:
                        # If P=[] and the current target indegree is lower or equal to 2,
                        # there is no reversible path in the graph
                        ExistsReversiblePath = False
                else:
                    # At least one reversible path is found and it has the highest target indegree
                    # We reverse the first in the list.
                    self.reverse_path(P[0])
                    # set target_indegree to the maximum indegree value
                    target_indegree = max(dict(self.in_degree()).values())
                    print(target_indegree)

    def plot(self, title='graph', node_color='b', edges_color='k'):

        ## todo : change here so we can keep the labels of the nodes
        G = networkx.convert_node_labels_to_integers(self)
        fig, ax = plt.subplots()
        pos = networkx.get_node_attributes(G, 'pos')
      #  edges_color = networkx.get_edge_attributes(G, 'color')
      #  edges_color = list(edges_color.values())

        # Todo : une fonction maison en 3D
        # En attendant on projette en 2D
        def projection2D(pos3D):
            return (pos3D[0], pos3D[1])
        posval = list(pos.values())
        posval = list(map(projection2D, posval))

        #Todo : allow choice of color
        networkx.draw_networkx(G, posval, alpha=.75, with_labels=True,
                               ax=ax, node_color=node_color, edge_color=edges_color)

        ax.title.set_text(title)


## Pour tester la classe
if __name__ == "__main__":

    test = "multigraph"
    if test == "hexagonal":
        ## Create a topology of two honeycomb networkd linked by a bridge
        Example = networkx.hexagonal_lattice_graph(4, 5)
        Example = networkx.convert_node_labels_to_integers(Exemple)
        nb_nodes = len(Exemple.nodes)
        Example.add_node(nb_nodes, pos=[-1, 0.5])
        Example.add_node(nb_nodes + 1, pos=[10, 0.5])
        Example.add_edge(nb_nodes, 1)
        Example.add_edge(nb_nodes + 1, 50)
        Example.remove_edges_from([(27, 37), (25, 35), (21, 31)])
        Example.remove_nodes_from([19, 29])

        topology = Topology(Exemple)

        G = Graph_orientation(topology)
        plotgraph(G, 'Initial graph', plot_format='2D_projection')
        G.set_orientation_Stronglyconnected()
        check = G.check_compatibility_flowsolver()
        if not check:
            color = 'r'
        else:
            color = 'b'
        plotgraph(G, 'Oriented graph', plot_format='2D_projection')

    elif test == "multigraph":

        from networkx.generators.atlas import graph_atlas

        def atlas():
            """ Return the atlas of all connected graphs of 6 nodes or less.
                Attempt to check for isomorphisms and remove.
                adapted from the script of author : Aric Hagberg (hagberg@lanl.gov)
            """
            Atlas = [graph_atlas(i) for i in range(208)]  # 208

            U = []  # list of all graphs in atlas
            for G in Atlas:
                # remove isolated nodes, only connected graphs are left
                zerodegree = [n for n in G if G.degree(n) == 0]
                for n in zerodegree:
                    G.remove_node(n)
                if G.nodes:
                    if (networkx.is_connected(G)):
                        U.append(G)

            # do quick isomorphic-like check, not a true isomorphism checker
            UU = []  # list of nonisomorphic graphs
            for G in U:
                # check against all nonisomorphic graphs so far
                if not iso(G, UU):
                    UU.append(G)
            return UU

        from networkx.algorithms.isomorphism.isomorph import graph_could_be_isomorphic as isomorphic

        def iso(G1, glist):
            """Quick and dirty nonisomorphism checker used to check isomorphisms."""
            for G2 in glist:
                if isomorphic(G1, G2):
                    return True
            return False

        def norm2D(pos):
            return np.sqrt(pos[0] ** 2 + pos[1] ** 2)

        def shift(pos, dx=0, dy=0):
            return (pos[0] + dx, pos[1] + dy)

        def plot_multigraph(graph_list, title='graph', plot_format='layout', node_color=None):
            print("the list of graph has %d gaphs"
                  % (len(graph_list)))

            N = np.sqrt(len(graph_list))

            plt.figure(figsize=(8, 8))

            for n, g in enumerate(graph_list):
                j, i = divmod(n, N)
                # layout graphs with positions using graphviz neato
                # edgelist=list(g.edges)
                # g.clear()
                # g.add_edges_from(edgelist)
                pos = graphviz_layout(g, prog="neato")
                posval = list(pos.values())
                # rescaling
                # max distance to (0,0):
                r_max = max(list(map(norm2D, posval)))
                posval = posval / r_max
                # shifting
                Delta = 3  # size of one case
                dx = Delta * (i + 0.5)
                dy = Delta * (j + 0.5)
                posval = list(
                    map(shift, posval, [dx] * len(posval), [dy] * len(posval)))

                for i, k in enumerate(pos.keys()):
                    pos[k] = posval[i]
                if node_color[n] is None:
                    # random color... peut trouver mieux ici
                    c = [np.random.random()] * networkx.number_of_nodes(g)

                    networkx.draw(g,
                                  pos,
                                  node_size=40,
                                  node_color=c,
                                  vmin=0.0,
                                  vmax=1.0,
                                  with_labels=False
                                  )
                else:
                    networkx.draw(g,
                                  pos,
                                  node_size=40,
                                  node_color=node_color[n],
                                  with_labels=False
                                  )
            plt.show()

        UU = atlas()
        U = []
        color = []
        bad_networks = []
        for i, G in enumerate(UU):
            print('\n Grap ' + str(i) + ':')
            nb_nodes = len(G.nodes) + 1
            G.add_node(nb_nodes)
            G.add_edge(nb_nodes, 0)
            topology = Topology(G)
            G = Graph_orientation(topology)
            G.set_orientation_Stronglyconnected()
            check = G.check_compatibility_flowsolver()
            if check == False:
                color.append('r')
                bad_networks.append(i)
            else:
                color.append(None)
            U.append(G)
        plot_multigraph(U, node_color=color)
    elif test == "unique_atlas":
        i = 5  # Choix de l''atlas
        UU = atlas()
        G = UU[i]
        print('\n Graph ' + str(i) + ':')
        nb_nodes = len(G.nodes)
        G.add_node(nb_nodes)
        G.add_edge(nb_nodes, 0)
        topology = Topology(G)
        G = Graph_orientation(topology)
        G.set_orientation_Stronglyconnected()
        check = G.check_compatibility_flowsolver()
        plotgraph(G, 'Graph ' + str(i))

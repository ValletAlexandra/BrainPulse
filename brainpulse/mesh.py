## To be rethought, the mesh should not depend on the problem.
#The number of parameters and the number of variables depends on the problem and should therefor be stored elsewhere
# Need to create a class variable as a long np array containing the cell values for all the vessels and then working with indexes.

import numpy as np


class Network(object):

    def __init__(self):
        self.vessels = []  # list of vessels
        self.nodes = []  # list of nodes
        self.innernodes = []  # list of nodes
        # mapping from  the node id provided in the user data to the node index in the mesh nodes list
        self.mappingnodes = {}
        # mapping from the edges (n1,n2) in the topology to the the vessel index in the mesh vessel list
        self.mappingedges = {}

        self.t = None

    def addNode(self, node):
        self.nodes.append(node)

    def addVessel(self, vessel):
        self.vessels.append(vessel)

    def addInnernode(self, node):
        self.innernodes.append(node)


class Vessel(object):

    def __init__(self):

        self.Id = None
        self.Parameters = {}  # dictionary of simulation parameters. {parameter:value}

        # grid size
        self.dx = None  # length of a cell
        self.L = None  # length of the vessel

        self.coords = None  # Coordinates of the points where to project the solution for 3D output
        self.radius0 = None
        self.s = None

        # Coordinates of the cells
        self.xim = None  # left face
        self.xip = None  # right face
        self.xi = None  # center of the cell

        # Ghost cells
        self.UghostL = None
        self.UghostR = None

        # Riemman invariant in ghost cells
        self.WghostL = None
        self.WghostR = None

        self.WghostLlast = None
        self.WghostRlast = None

        # Ghost cells
        self.FghostL = None
        self.FghostR = None

        self.Au = None
        self.sAu = None
        self.invAu = None

        # Iterative solution in cells
        self.U = None

        # Iterative solution at faces
        self.UL = None
        self.UR = None

        # Iterative fluxes at faces
        self.FL = None
        self.FR = None

        # Iterative Flux Arrays
        self.Flux = None

    def setId(self, id):
        self.Id = id

    def setCoords(self, coords):
        self.coords = np.array(coords, np.float64)

    def setRadius(self, listradius):
        self.radius0 = np.array(listradius, np.float64)

    def setCurvilinearCoordinate(self, s):
        self.s = np.array(s, np.float64)

    def InitParameters(self, E, gamma, rho, A0, AL, L, Cf):
        self.Parameters['dAdx'] = (A0-AL)/L
        self.Parameters['c0'] = np.sqrt(E*gamma / (1 - n ** 2) / 2 / rho)
        self.Parameters['coeffC'] = 2 * A0 * (1 - n ** 2) / E / gamma
        self.Parameters['coeffL'] = rho / A0
        self.Parameters['coeffR'] = rho * Cf / (A0 ** 2)
        self.Parameters['Z'] = self.Parameters['c0'] * rho / A0


class Node(object):

    def __init__(self):
        self.Id = None
        self.Parents = []  # list of vessels
        self.Daughters = []  # list of vessels

    def setId(self, id):
        self.Id = id

    def addParent(self, vessel):
        self.Parents.append(vessel)

    def addDaughter(self, vessel):
        self.Daughters.append(vessel)

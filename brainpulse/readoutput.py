# -*- coding: utf-8 -*-

import re
import numpy as np
import matplotlib.pyplot as plt


class pathreader (object):

    def __init__(self, file):
        with open(file, 'r') as f:
            line = f.readline()
            t = []
            while line:
                if line[0:8] == '# Curvil':
                    line = f.readline()
                    xi = [n for n in map(float, line.split(','))]

                elif line[0:8] == '# Variab':
                    line = f.readline()
                    line = line.strip()
                    var = [n for n in map(str, line.split(','))]

                elif line[0:8] == '# time :':
                    t.append(float(re.findall(r"\d*\.\d+|\d+", line)[0]))

                line = f.readline()
            f.close()

        self.t = np.array(t)
        self.xi = np.array(xi)
        self.variables = {}
        for i, v in enumerate(var):
            self.variables[v] = i

        with open(file, 'r') as f:
            Dataraw = np.loadtxt(
                f, dtype='float', comments='# ', delimiter=',', converters=None, skiprows=8)
            f.close()

        # reshape 2D data to 3D data

        self.data = np.zeros((len(self.variables), len(self.xi), len(self.t)))

        for iline in range(0, Dataraw.shape[0]):
            k, i = divmod(iline+1, len(self.variables))
            self.data[i-1, :, k-1] = Dataraw[iline, :]

        print('Loading data from file %s' % file)
        print('Found %i variables, %i cells, %i times' % self.data.shape)

    def spatialplot(self, t, var, *args, xscale=1, yscale=1, ** kwargs):
        x = self.xi
        it = np.where(abs(self.t-t) == min(abs(self.t-t)))[0][0]
        iva = self.variables[var]
        y = self.data[iva, :, it].reshape(-1)

        plt.plot(x*xscale, y*yscale, *args, **kwargs)
        plt.xlabel('curvilinear coordinate (cm)')
        plt.ylabel(var + '(cgs unit)')

    def timeplot(self, xi, var, *args, xscale=1, yscale=1, ** kwargs):
        x = self.t
        ix = np.where(abs(self.xi-xi) == min(abs(self.xi-xi)))[0][0]
        iva = self.variables[var]
        y = self.data[iva, ix, :].reshape(-1)

        plt.plot(x*xscale, y*yscale, *args, **kwargs)
        plt.xlabel('time (s)')
        plt.ylabel(var + '(cgs unit)')

    def show(self):
        plt.show()


if __name__ == "__main__":
    file1 = '/home/alexandra/outputs/Pulse_windkessel/pathdata/path_0-1.dat'
    file2 = '/home/alexandra/outputs/Pulse_doublevessel/pathdata/path_0-1.dat'

    mmHg = 0.00075006375541921

    import numpy as np
    rho = 1
    Cf = 100
    R0 = 0.5
    A0 = np.pi*R0**2
    coeffR = rho * Cf / (A0 ** 2)
    Q = 300

    pathreader1 = pathreader(file1)
    pathreader2 = pathreader(file2)

    plt.figure(1)
    pathreader1.spatialplot(4.5, 'P', '*')
    pathreader2.spatialplot(4.5, 'P', '*')

    plt.show()

    plt.figure(1)
    pathreader1.timeplot(3, 'P', '*-')
    pathreader2.timeplot(3, 'P', '*-')

    plt.show()

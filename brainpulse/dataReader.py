from abc import ABCMeta, abstractmethod
import csv
import ast

## Modifier en utilisant le design pattern de Factory


class DataReader(object):
    """ Context block of the strategy pattern for the creation of a topology """
    ## Todo et si  plus tard on souhaite ajouter une autre propriete utile ici? Il faudrait une liste d'elements a construire et une methode pour chaue element dans des classes.

    def __init__(self, type=None, *args, **kwargs):
        self._strategy = parse_reader(dataReaders, type)

    def get_data(self, *args, **kwargs):
        data = self._strategy.get_data(self, *args, **kwargs)
        return data


class DataReaderStrategy(metaclass=ABCMeta):
    """Abstract base strategy class for the methods of getting data."""

    def __init__(self, type):
        self._readerstrategy = type

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def description(self):
        pass

    @abstractmethod
    def get_data(self, *args, **kwargs):
        """Get data depending on the strategy"""


class DataReaderNotKnown(DataReaderStrategy):
    name = "Unknown"
    description = "Unknown method"

    def get_data(self, *args, **kwargs):
        print("Type not known, please see documentation")
        exit(1)


class DataReaderAuto(DataReaderStrategy):
    name = "Automatic"
    description = "The data will be generated automatically"

    def get_data(self, *args, **kwargs):
        return None


class DataReaderInLine(DataReaderStrategy):
    name = "In line"
    description = "The data is extracted from the input file."

    def get_data(self, value, *args, **kwargs):
        data = {}
        for v in value:
            data[v] = ast.literal_eval(str(value[v]))
        return data


class DataReaderAll(DataReaderStrategy):
    name = "All"
    description = "One value given in the input file."

    def get_data(self, value, *args, **kwargs):
        return [ast.literal_eval(str(value))]


class DataReaderTxtFiles(DataReaderStrategy):
    name = "Text files"
    description = "The data is extracted from a text file."

    def get_data(self, file, *args, **kwargs):

        fname = file
        with open(fname, 'rt', newline='') as file:
            has_header = csv.Sniffer().has_header(file.read(1024))
            file.seek(0)  # Rewind.
            reader = csv.reader(file, delimiter=',')
            if has_header:
                next(reader)  # Skip header row.
            data = {}
            for row in reader:
                for i, n in enumerate(row):
                    row[i] = ast.literal_eval(n)
                if len(row) == 2:
                    data[row[0]] = row[1]
                else:
                    data[row[0]] = tuple(row[1:])
            file.close()

        return data


# To update if including a new reader
dataReaders = {"file": DataReaderTxtFiles, "list": DataReaderInLine,
               "auto": DataReaderAuto, "all": DataReaderAll}


def parse_reader(dataReaders, reader_type):
    strategy = dataReaders.setdefault(reader_type, DataReaderNotKnown)
    return strategy


if __name__ == "__main__":

    config = {
        'vessels':
            {
                'topology': {'type': 'from data',
                             'edges': {'type': 'list', 'value': {0: '0 , 1', 1: '1 , 2'}},
                             'nodes': {'type': 'auto'},
                             'lengths': {'type': 'list', 'value': {0: 1, 1: 1}, 'unit': 'cm'}}
            }
    }

    Reader = DataReader(**config['vessels']['topology']['nodes'])
    nodes = Reader.get_data(**config['vessels']['topology']['nodes'])

    Reader = DataReader(**config['vessels']['topology']['edges'])
    edges = Reader.get_data(**config['vessels']['topology']['edges'])
    print(edges)
    Reader = DataReader(**config['vessels']['topology']['lengths'])
    lengths = Reader.get_data(**config['vessels']['topology']['lengths'])

    Reader = DataReader(type='file')
    data = Reader.get_data(file='../data/exemple_data.txt')
    print(data)

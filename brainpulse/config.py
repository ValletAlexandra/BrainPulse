import logging


def ReadConfig(config_file, logger=None):
    """Read in a configuration file and return the corresponding dicts.

    Parameters:
        config_file:    The name of the configuration file to read.

    Returns:
        list of config dicts
    """

    import yaml

    # without configuring dummy before.
    logger = logger or logging.getLogger('dummy')
    logger.name = 'ReadConfig in config'
    logger.info('Reading YAML config file %s', config_file)

    with open(config_file) as file:
        # The FullLoader parameter handles the conversion from YAML
        # scalar values to Python the dictionary format
        config = yaml.load(file, Loader=yaml.FullLoader)

    return config

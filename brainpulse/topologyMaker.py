# -*- coding: utf-8 -*-

from brainpulse.topology import Topology
from brainpulse.dataReader import DataReader
from abc import ABCMeta, abstractmethod
import csv
import networkx
import brainpulse.utils as utils

## Modifier en utilisant le design pattern de Factory


def buildTopology(vessels=None, *args, **kwargs):
    builder = parse_topology(topologyReaders, vessels['topology']['type'])()
    Topology = builder.build(**vessels['topology'])
    #Assign the properties to the nodes (Radius, Young modulus, etc)
    #todo : assign properties to the edges or nodes
    Topology.set_properties(vessels['properties'])
    return Topology


class TopologyCreationStrategy(metaclass=ABCMeta):
    """Abstract base strategy class for the methods of getting nodes and edges from the data."""

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def description(self):
        pass

    @abstractmethod
    def build(self, *args, **kwargs):
        """Build an object topology"""


class TopologyCreationNotKnown(TopologyCreationStrategy):
    name = "Unknown"
    description = "Unknown method"

    def build(self, *args, **kwargs):
        print("ERROR : No topology reader named %s" % self._topologyreadertype)
        exit(1)


class TopologyCreationNxGraph(TopologyCreationStrategy):
    name = "Networkx graph"
    description = "The topology is created from a graph object from the Networkx library"

    def build(self, G, key_pos=None, key_edgeid=None, key_length=None, *args, **kwargs):
        """ Concrete method for the build() abstract method in the
        TopologyCreationStrategy abstract class.
        Is used in the build() method in the TopologyBuilder class.

        Get the nodes and edges data from a graph created with the Networkx library

                :argument H: graph object created with the networkx library
                :param coordinates: key word of the nodes attributes that should be used to define the nodes coordinates
                :type H : Graph or DiGraph
                :type key_pos : str
                :return : two list of dict describing the nodes and the edges with the following formalism :
                dict of nodes : {id : int , pos : tuple of size 3},
                dict of edges : {id : int or str , e : tuple of size 2}

            :Exemple:

                G=nx.hexagonal_lattice_graph(4, 5)

                topologyBuilder = TopologyBuilder(type = "Nx Graph")
                Topologie=topologyBuilder.build(G,key_pos='pos')


        """

        ## Extract data from the graph object and generate missing data

        ## todo : remove this and verify it is working, in order to keep the node labels that are used to specify the BC
        #H=networkx.convert_node_labels_to_integers(G.to_undirected())
        H = G.to_undirected()

        nodes_list = H.nodes
        edges_list = H.edges

        if not key_pos:
            #If no key for the coordinate attributes is given, the coordinates are evaluated using
            #the graphiz_layout method.
            from networkx.drawing.nx_pydot import graphviz_layout
            #In such a case, the 'pos' keyword should not be attached to the nodes
            for n in H.nodes:
                try:
                    H.nodes[n].pop('pos')
                except:
                    pass
            pos_list = graphviz_layout(H, prog="neato")
            pos_list = [pos_list[n] for n in nodes_list]
        else:
            pos_list = []
            for n in nodes_list:
                try:
                    node_pos = H.nodes[n].pop(key_pos)
                except:
                    #todo put in the logger
                    print("The node %i does not have any %s attribute" %
                          (n, key_pos))
                    print("Default position (%d,%d,%d) attributed" % (0, 0, 0))
                    node_pos = (0, 0, 0)

                if len(node_pos) == 2:
                    node_pos = (node_pos[0], node_pos[1], 0)
                elif len(node_pos) == 1:
                    node_pos = (node_pos[0], 0, 0)
                elif len(node_pos) == 0:
                    # todo put in the logger
                    print("The node %i does not have any %s value" %
                          (n, key_pos))
                    print("Default position (%d,%d,%d) attributed" % (0, 0, 0))
                    node_pos = (0, 0, 0)
                elif len(node_pos) > 3:
                    # todo put in the logger
                    print("Too much dimensions in the %s value" % (n, key_pos))
                    print("The position was truncated to the first 3 elements")
                    node_pos = (node_pos[0], node_pos[1], node_pos[2])

                pos_list.append(node_pos)

        if not key_edgeid:
            #If no key for the edge ID attributes is given, the IDs are incremental integers
            ids_list = range(len(edges_list)+1)
        else:
            ids_list = []
            for i, e in enumerate(edges_list):
                try:
                    edge_id = H.edges[e][key_edgeid]
                except:
                    #todo put in the logger
                    print("The edge (%i,%i) does not have any %s attribute." %
                          (e[0], e[1], key_pos))
                    edge_id = i
                ids_list.append(edge_id)

        ## Todo : rewrite the function here, I dont understand the need of the lists before
        graph = Topology()

        for node, pos in zip(nodes_list, pos_list):
            graph.add_node(node, pos)

        for edge, edgeid in zip(edges_list, ids_list):
            graph.add_edge(edge[0], edge[1], id=edgeid)

        if not key_length:
            if not key_pos:
                print('ERROR : At least the position of nodes or the lengths of vessels should be attached to nodes or edges respectively')
                exit(1)
            else:
                for e in graph.edges:
                    length = utils.distance(
                        graph.nodes[e[0]]['pos'], graph.nodes[e[1]]['pos'])
                    graph.edges[e]['length'] = length
        else:
            for e in H.edges:
                try:
                    length = H.edges[e][key_length]
                except:
                    #todo put in the logger
                    print("ERROR : The edge (%i,%i) does not have any %s attribute." % (
                        e[0], e[1], key_length))
                    exit(1)
                graph.edges[e]['length'] = length

        return graph


class TopologyCreationFromData(TopologyCreationStrategy):
    name = "From data"
    description = "The topology is created from data given by the user"

    def build(self, edges, nodes=None, lengths=None, *args, **kwargs):

        if (not(nodes)) & (not(lengths)):
            print(
                "ERROR : The user should at least provide nodes coordinates data or lengths data")
            exit(1)

        #Create a graph describing the vessels network topology
        graph = Topology()

        # Get the network edges and build the graph structure
        Reader = DataReader(**edges)
        edgesdata = Reader.get_data(**edges)

        for e in edgesdata:
            graph.add_edge(edgesdata[e][0], edgesdata[e][1], id=e)

        # todo : Here we need to take into account for the unit. Should create a function convert_to_cgs
        # Get the node coordinates and set as node attributes in the graph
        if nodes:
            Reader = DataReader(**nodes)
            nodesdata = Reader.get_data(**nodes)
        else:
            nodesdata = []

        if nodesdata:
            #Checking coherency in the number of coordinates provided
            if len(nodesdata) != len(graph.nodes):
                print(
                    'ERROR : The number of node coordinates provided do not corresponds to the number of nodes')
                exit(1)
            for n in nodesdata:
                graph.nodes[n]['pos'] = nodesdata[n]
        else:
            # If no node coordinates are given, they are generated using the graphiz_layout method.
            # todo : check if it is working even if the node numbering is not continuous
            # todo :  mettre en 3D
            from networkx.drawing.nx_pydot import graphviz_layout
            pos_list = graphviz_layout(graph, prog="neato")
            for n in graph.nodes:
                graph.nodes[n]['pos'] = (pos_list[n][0], pos_list[n][1], 0)

        # If apparent lengths are provided for the edges, then the length attribute is overwriten
        # todo : allow to provide length for fiew vessels only
        if lengths:
            Reader = DataReader(**lengths)
            lengthsdata = Reader.get_data(**lengths)
            #Checking coherency in the number of lengths provided
            if len(lengthsdata) != len(graph.edges):
                #todo : allow to provide lengths for fiew vessels only, if node positions is given
                print(
                    'ERROR : The number of lenghts provided do not corresponds to the number of edges')
                exit(1)
            for l in lengthsdata:
                idEdge = graph.edgesID[l]
                graph.edges[idEdge]['length'] = lengthsdata[l]
        else:
            # If no lengths are given, they are calculated based on the node coordinates
            for e in graph.edges:
                length = utils.distance(
                    graph.nodes[e[0]]['pos'], graph.nodes[e[1]]['pos'])
                graph.edges[(e[0], e[1])]['length'] = length

        return graph


# To update when adding a new reader
topologyReaders = {"from data": TopologyCreationFromData,
                   "Nx Graph": TopologyCreationNxGraph}


def parse_topology(topologyReaders, topology_type):
    strategy = topologyReaders.setdefault(
        topology_type, TopologyCreationNotKnown)
    return strategy


if __name__ == "__main__":

    config = {'simulation':
              {'job': 'simple artery', 'test_case': False, 'case': 'none'},
              'solver': {'type': 'linear', 'CFL': 0.7, 'tfinal': 1},
              'output': {'freq': 0.1, 'dir': '/home/alexandra/outputs/'},
              'problem': {'csf_pressure': {'type': 'constant', 'value': 0, 'unit': 'mmHg'},
                          'blood_viscosity': {'type': 'constant', 'value': '1e-2', 'unit': 'tutu'},
                          'blood_density': {'type': 'constant', 'value': 0.1, 'unit': 'g/cm3'}},
              'vessels': {'topology':
                          {'type': 'from data',
                           'edges': {'type': 'list', 'value': {0: '0 , 1', 1: '1 , 2'}},
                           'nodes': {'type': 'auto'},
                           'lengths': {'type': 'list', 'value': {0: 1, 1: 1}, 'unit': 'cm'}},
                          'properties':
                          {'young_modulus': {'type': 'all', 'value': '1e3', 'unit': 'cgs'},
                              'radius': {'type': 'list', 'value': {0: '50e-4', 1: '60e-4', 2: '80e-4'}, 'unit': 'cgs'},
                              'gamma': {'type': 'all', 'value': 0.1}},
                          'boundary_conditions': [{'type': 'pressure', 'function': 'constant', 'value': '1e3', 'unit': 'cgs', 'nodes': 0}, {'type': 'flux', 'function': 'constant', 'value': 50, 'unit': 'csg', 'nodes': 1}], 'initial_conditions': [{'type': 'pressure', 'function': 'constant', 'value': '1e3', 'unit': 'cgs', 'edges': '0,1'}, {'type': 'flux', 'function': 'constant', 'value': 50, 'unit': 'csg', 'edges': '1,2'}], 'mesh': {'dx': None}}}

    import networkx as nx
    G = nx.hexagonal_lattice_graph(4, 5)

    topologyBuilder = TopologyBuilder(type="Nx Graph")
    Topo = topologyBuilder.build(G, key_pos='pos')

    topologyBuilder = TopologyBuilder(**config['vessels']['topology'])
    Topo = topologyBuilder.build(**config['vessels']['topology'])

    G = nx.Graph()
    G.add_edges_from([(0, 1), (2, 1), (2, 3), (3, 4),
                     (4, 5), (5, 6), (3, 7), (7, 8), (9, 8)])
    nx.set_edge_attributes(G, 1, 'L')
    topologyBuilder = TopologyBuilder(type="Nx Graph")
    Topo = topologyBuilder.build(G, key_length='L')
    Topo.plot(title='Initial graph')
    for e in Topo.edges:
        print(Topo.edges[e]['length'])

    Topo.simplify()

    Topo.plot(title='simplified graph')
    for e in Topo.edges:
        print(Topo.edges[e]['length'])

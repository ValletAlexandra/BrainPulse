import numpy as np
import networkx
from abc import ABCMeta, abstractmethod
import os
import vtk
from vtk.util import numpy_support

#todo : error message if the nodes specified for the path do not allow to build  apath (non existing, or idendical nodes)

class SaveData(object):

    def __init__(self,vessel, timepoints=[0]):
        '''
        Constructor
        '''
        self.v=vessel
        self.t = timepoints
        self.s = vessel.xi
        self.P = np.zeros([len(timepoints), len(vessel.xi)])
        self.Q = np.zeros([len(timepoints), len(vessel.xi)])
        self.A = np.zeros([len(timepoints), len(vessel.xi)])


def outputs(MeshObj,TopologyObj,SolverObj,output=None,job_name='noname',*args, **kwargs):
    output_list = []
    if output :
        # Check if the output folder exists and create it if not
        dir=output['dir']
        if not os.path.exists(dir):
            os.makedirs(dir)

        ## Create a folder for the simulation in the output folder
        dir=dir+'/'+job_name

        if not os.path.exists(dir):
            os.makedirs(dir)

        #construct an output writter object for each output format specified
        for format in output['format']:
            #construct a dict with the variables asked by the user to be saved
            vardict={}
            for var in format['variables']:
                vardict[var]=SolverObj.variables[var]
            #construct the output writer object depending on the type selected by the user
            if format['type']=='path' :
                ## Create a folder for the path output
                dirpath = dir + '/'+'pathdata'

                if not os.path.exists(dirpath):
                    os.makedirs(dirpath)
                out=path_writter(MeshObj,TopologyObj,dir=dirpath,vardict=vardict,**format)
            elif format['type']=='paraview' :
                ## Create a folder for the path output
                dirparaview = dir + '/'+'paraview'

                if not os.path.exists(dirparaview):
                    os.makedirs(dirparaview)
                out=paraview_writter(MeshObj,dir=dirparaview,vardict=vardict,**format)
            else :
                print('ERROR : the output format %s is not known')
                exit(1)
            output_list.append(out)
    else :
        print('No output specified in the config file')

    return output_list

class output_writter(metaclass=ABCMeta) :

    def __init__(self,vardict={},dir='',period=0,*args, **kwargs):
        self.dir=dir
        self.period= period
        self.nexttimebackup=0
        self.variables=vardict # dict with the method to calculate each of the output variables from the mesh variables (depend on the solver)

    @abstractmethod
    def write(self, *args,**kwargs):
        """Write output"""

class path_writter(output_writter):

    def __init__(self,mesh,topology,source=None, target=None,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.file=None
        self.source=source
        self.target=target
        self.vessels=[] # list of the vessels along the path
        self.orientations=[] # list of the orientation of the vessel along the path
        self.xi=[]
        if (source is not None) & (target is not None) :
            self.filename='path_%i-%i'%(source,target)
            self.set_path(mesh,topology,source, target)
            self.create_file()
        else :
            print('ERROR : the source or the target nodes in the path format is missing ')

    def write(self,mesh):
        print('Write data in path output format at time : %e s'%mesh.t)
        self.file.write('# time : '+str(mesh.t)+'\n')
        for var in self.variables :
            var_value=[]
            for i,v in enumerate(self.vessels):
                ary=self.variables[var](v)
                if self.orientations[i]==1:
                    var_value=var_value+list(ary)
                else :
                    var_value = var_value + list(np.flip(ary))
            self.file.write(','.join(['%.9e' % x for x in var_value])+'\n')
        self.file.write('\n')
        self.nexttimebackup+=self.period

    def set_path(self,mesh,topology,source, target):
        G = topology.to_undirected()
        try :
            path = networkx.shortest_path(G, source, target)
        except:
            print('ERROR: couldnt build the parth %i - %i. Please check that source and target are boundary nodes or junction nodes in the topology')
            exit(1)
        xi=[]
        L=0
        for i in range(1, len(path)):
            e=(path[i-1],path[i])
            if e in topology.edges :
                iv=mesh.mappingedges[e]
                self.orientations.append(1)
            else :
                iv=mesh.mappingedges[(e[1],e[0])]
                self.orientations.append(-1)
            self.vessels.append(mesh.vessels[iv])
            xi=xi+list(np.array(mesh.vessels[iv].xi)+L)
            L=L+mesh.vessels[iv].L
        self.xi=xi

    def create_file(self):
        self.file = open(self.dir+'/'+self.filename+'.dat', 'w')
        self.file.write('# Path data between source node %i and target node %i \n\n'%(self.source,self.target))
        self.file.write('# Variables : \n')
        self.file.write(','.join(['%s' % va for va in self.variables])+'\n\n')
        self.file.write('# Curvilinear coordinate : \n')
        self.file.write(','.join(['%.3e' % xi for xi in self.xi])+'\n\n')

class paraview_writter(output_writter):

    def __init__(self,mesh,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.writer = vtk.vtkXMLPolyDataWriter()
        self.counter=0
        self.points = vtk.vtkPoints()
        self.line = vtk.vtkCellArray()


        for v in mesh.vessels:
            nbPointsVTK = self.points.GetNumberOfPoints()

            x_mesh=np.concatenate((v.xim, [v.L]))

            x = np.interp(x_mesh, v.s, [c[0] for c in v.coords])
            y = np.interp(x_mesh, v.s, [c[1] for c in v.coords])
            z = np.interp(x_mesh, v.s, [c[2] for c in v.coords])

            nbPoints=len(x_mesh)

            self.points.InsertNextPoint([x[0],y[0],z[0]])

            for i in range(1,nbPoints):
                self.points.InsertNextPoint([x[i],y[i],z[i]])
                self.line.InsertNextCell(2)
                self.line.InsertCellPoint(nbPointsVTK+i-1)
                self.line.InsertCellPoint(nbPointsVTK+i)



        self.polydata = vtk.vtkPolyData()
        self.polydata.SetPoints(self.points)
        self.polydata.SetLines(self.line)

        nbCellsVTK = self.line.GetNumberOfCells()

        for var in self.variables:
            vtkArray=vtk.vtkFloatArray()
            vtkArray.SetNumberOfComponents(1)
            vtkArray.SetNumberOfTuples(nbCellsVTK)
            vtkArray.SetName(var)
            self.polydata.GetCellData().AddArray(vtkArray)
            self.polydata.Modified()


    def write(self,mesh):
        print('Write data in paraview output format at time : %e s'%mesh.t)
        for var in self.variables:
            self.polydata.GetCellData().SetActiveScalars(var)
            array=np.array([])
            for v in mesh.vessels:
                array=np.concatenate((array,self.variables[var](v)))
            vtkArray = numpy_support.numpy_to_vtk(array)
            vtkArray.SetName(var)
            self.polydata.GetCellData().SetScalars(vtkArray)
        self.polydata.Modified()

        self.writer.SetFileName(self.dir+'/output'+str(self.counter)+'.vtp');
        if vtk.VTK_MAJOR_VERSION <= 5:
            self.writer.SetInput(self.polydata)
        else:
            self.writer.SetInputData(self.polydata)
        self.writer.Write()
        self.counter+=1
        self.nexttimebackup += self.period


import abc
import numpy as np
import time
from abc import ABCMeta, abstractmethod


# Context
class SolverContext(object):

    def __init__(self, MeshObj, SolverObj, BCObj, OuputObj, solver=None, *args, **kwargs):
        self.mesh = MeshObj
        self.dt = SolverObj.computedt(MeshObj, solver['CFL'])
        self.solver = SolverObj
        self.boundaryconditions = BCObj
        self.t = 0
        self.tend = solver['tfinal']
        self.Outputs = OuputObj
        self.initialize()

        print('The time step is :', self.dt)

    def initialize(self):
        self.t = 0
        self.solver.init(self.mesh)

    def run(self):

        start_time = time.time()

        while self.t <= self.tend:
            for out in self.Outputs:
                if self.t >= out.nexttimebackup:
                    out.write(self.mesh)

            self.solver.advancetime(
                self.mesh, self.boundaryconditions, self.dt, self.t)
            self.t += self.dt
        elapsed_time = time.time() - start_time
        print('Simulation total run time : ', elapsed_time)


def Solver(solver=None, *args, **kwargs):
    if solver['name'] == 'BFLinearSolver':
        from brainpulse.BFLinearSolver import BFLinearSolver
        SolverObj = BFLinearSolver(**solver)
        return SolverObj
    else:
        print('ERROR, the solver %s is not known' % solver['name'])
        exit(1)


# AbstractStrategy
class AbsSolver(metaclass=abc.ABCMeta):

    def __init__(self, type):
        self._topologyreadertype = type

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def description(self):
        pass

    @property
    @abstractmethod
    def mesher(self):
        pass

    @property
    @abstractmethod
    def parameters(self):
        pass

    @property
    @abstractmethod
    def parameters_assignement(self):
        pass

    @property
    @abstractmethod
    def nbVariables(self):
        pass

    @abc.abstractmethod
    def advancetime(self):
        """ update the field for the next time step"""

    @abc.abstractmethod
    def computedt(self):
        """ compute the time step """

    @abc.abstractmethod
    def init(self):
        """ initialise the variables """

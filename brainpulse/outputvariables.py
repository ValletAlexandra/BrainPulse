## module related to the BFLinearSolver
# provide the functions to compute output variables for a vessel

import numpy as np


def P(v):
    return v.U[0, :]


def Q(v):
    return abs(v.U[1, :])


def A(v):
    return (v.Parameters['A0']*v.Au)*(v.U[0, :]*v.Parameters['coeffC']/(2*v.Parameters['A0'])+1)**2


def R(v):
    return np.sqrt((v.Parameters['A0'] * v.Au) * (v.U[0, :] * v.Parameters['coeffC'] / (2 * v.Parameters['A0']) + 1) ** 2/np.pi)

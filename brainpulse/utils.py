# -*- coding: utf-8 -*-


import numpy as np
from math import sqrt

def distance (n,p) :
    res = sqrt(sum([(n[i]-p[i])**2 for i in range(3)]))
    return res

class Items(object) :
    """ Class to create an Items object. 
    
    Add a new variable to the field 
    Field.add_variable
    
    Access to the field value using its index 
    Field.value[1]
    
    Access to the field value using the variable name
    Field.vvalue["var"]
    
    Access to the variable description using the variable name
    Field.helper["var"]
    
    
    """
    
    
    def __init__(self,value=np.array([]),varname=np.array([]),helper= {},unit={}):
        self.value=np.array(value)
        self.varname=np.array(varname)
        self.helper=helper
        self.unit=unit
        
        self.vvalue={}
        self.vindex={}

        
        self.update_variables()
        
    def __getitem__(self, name):
        """obj[name] syntax for getting value."""
        if isinstance(name, (list,tuple,)): # get many?
            return [self.vvalue[n] for n in name]
        else:
            return self.vvalue[name]
    
    def __setitem__(self, name, value):
        """obj[name] = value syntax"""
        self.value[self.vindex[name]]=value
                
    def add_variable(self,varname='var',value=0,helper='unknown',unit='unknown'):
        ##Todo mettre un message d'erreur si la va existe deja
        self.value=np.append(self.value,value)
        self.varname=np.append(self.varname,varname)
        self.helper[varname]=helper
        self.unit[varname]=unit
        self.updates()
                
    def update(self):
        for i in range(0,np.size(self.value)) :
            self.vvalue[self.varname[i]]=self.value[i:i+1]
            self.vindex[self.varname[i]]=i
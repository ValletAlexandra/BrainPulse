## module related to the BFLinearSolver
# provide the functions to compute parameters of a vessel

import numpy as np


def funA0(properties, dx):
    R0 = np.mean(properties['radius'])
    return np.pi * R0 ** 2


def func0(properties, dx):
    E = (properties['young_modulus'])
    n = (properties['poisson_coefficient'])
    rho = (properties['blood_density'])
    gamma = (properties['ratio_thickness_radius'])
    return np.sqrt(E*gamma/(1 - n**2)/2/rho)


def funcoefC(properties, dx):
    A0 = np.pi * np.mean(properties['radius']) ** 2
    E = (properties['young_modulus'])
    n = (properties['poisson_coefficient'])
    gamma = (properties['ratio_thickness_radius'])
    return 2 * A0 * (1 - n ** 2) / E / gamma


def funcoefL(properties, dx):
    A0 = np.pi * np.mean(properties['radius']) ** 2
    rho = (properties['blood_density'])
    return rho / A0


def funcoefR(properties, dx):
    A0 = np.pi * np.mean(properties['radius']) ** 2
    rho = (properties['blood_density'])
    Cf = (properties['friction_coef'])
    return rho * Cf / (A0 ** 2)


def funAlpha(properties, dx):
    A0 = np.pi * np.mean(properties['radius']) ** 2
    Cf = properties['friction_coef']
    return Cf / A0


def funZ(properties, dx):
    A0 = np.pi * np.mean(properties['radius']) ** 2
    E = (properties['young_modulus'])
    n = (properties['poisson_coefficient'])
    rho = (properties['blood_density'])
    gamma = (properties['ratio_thickness_radius'])
    return np.sqrt(E*gamma/(1 - n**2)/2/rho)*rho/A0


def funCi(properties, dx):
    A0 = np.pi * np.mean(properties['radius']) ** 2
    E = (properties['young_modulus'])
    n = (properties['poisson_coefficient'])
    rho = (properties['blood_density'])
    Cf = (properties['friction_coef'])
    gamma = (properties['ratio_thickness_radius'])
    return 1/(1+dx/2*(rho * Cf / (A0 ** 2))/(np.sqrt(E*gamma/(1 - n**2)/2/rho)*rho/A0))/(np.sqrt(E*gamma/(1 - n**2)/2/rho)*rho/A0)

# BrainPulse

This python code allows to solve the 1D reduced Navier-Stokes equations that describe fluid flow in deformable tubes. It is based on finite volume method and utilizes Riemann invariants to solve the hyperbolic equations. Special attention has been given to conditions at the junctions of a network of tubes, particularly for the management of the source term, to ensure mass conservation. Currently, only blood modeling is implemented. The code will soon be extended to include coupling with cerebrospinal fluid (CSF).

In the fully prepared environment a user needs to provide nodes and edges data describing the vascular system and set up configuration parameters by editing the default ‘example_config.yml’ file in the data folder.

Please see the documentation :

https://valletalexandra.gitlab.io/BrainPulse/


## Table of contents
* [Installation](#installation)
  * [Environment Preparation](#environment-preparation)
  * [Installation of BrainPulse Components](#installation-of-brainpulse-components)
* [BrainPulse Usage](#usage)
* [Authors](#authors)
* [License](#license)
* [Acknowledgments](#acknowledgments)

# Installation
BrainPulse is implemented as a set of python scripts that preprocess the data in order to create a mesh from topological data, run a solver to solve a system of partial differential equations and write outputs in a specific sequence with specific set of parameters. It can be installed (and used) as a set of scripts and third party required libraries installed directly in Linux OS.


## Environment preparation
Notice that below steps refer to Linux OS. However more experienced user can also use this manual to install BrainPulse on OSX or other Unix OS but it requires some slight adaptations that are not provided in the description below.

To complete the installation process BrainPulse requires the following components installed on your OS:

- Conda - an open source package management system
- Python 3.x
- git

If you are sure all of the above is working correctly (*R*,  *conda*, *Java*, *python 2.x*) on your system you can skip the next section and go to the section [Installation of BrainPulse Components].


### Conda
Check if there is conda installed on your machine:
```bash
conda info
```
If conda *command not found* please install it. Download anaconda from the web:
```bash
wget https://repo.anaconda.com/archive/Anaconda3-2020.11-Linux-x86_64.sh
```
Install it in the following directory: '*/opt/anaconda*' and remove the installation file.
```bash
sudo bash Anaconda3-2020.11-Linux-x86_64.sh -b -p /opt/anaconda
rm Anaconda3-2020.11-Linux-x86_64.sh 
```
Now, conda should be available from a terminal window. Open the new one and type again:
```bash
conda info
```
You should see all information about conda environment.

### Python 3.x
Check your python version. 
```bash
python3 --version
```
If your OS lacks of python 3.x please install it with the following commnads:
```bash
sudo apt-get update
sudo apt-get install python3
```

## Installation of BrainPulse

To get BrainPulse from the github repository you may download it as a zip file or clone the project:
```bash
git clone https://gitlab.com/ValletAlexandra/BrainPulse
cd BrainPulse
```

### Installation using conda environment
To install all required packages with conda run : 

```bash
conda env create -f environment_brainpulse.yml
```
All required packages and files should be installed or updated automatically and if that succeeded there is no need of any manual installation presented later below. If there is any missing component and BrainPulse stops with appropriate warning you may take a look at a specific section 'Required Packages'. In that case please also try to rerun the script in a terminal. Please notice that total size of packages is several GB and it can take a few minutes to download them.


### Required packages
With the provided conda environment file, conda should install the following packages:
  - graphviz
  - matplotlib
  - networkx
  - numpy
  - scipy
  - vtk
  - pyyaml

You can install them manually using pip if you don't want to use a conda environment.

In addition make sure to have tho following dependencies : 

apt install libgl1-mesa-glx


# BrainPulse usage

## Set up the configuration file

Before running a simulation, you need to configure an input file and provide topological data. Few examples are provided in the data folder.

### File 'example_config.yml'
The file '*example_config.yml*' contains BrainPulse input parameters. The default settings look like below:

### File 'Pulse-oneartery.yml'
The file '*Pulse-oneartery.yml*' contains the parameters ato run a blood flow simulation in one vessel.


## Set up the environment

Before running BrainPulse, you need to :

- Activate the conda environment (if you are using a conda environment) :
```bash
conda activate BrainPulse
```

- Add BrainPulse module to the python path using the '*setup.rc* file :
```bash
source setup.rc
```


## Run BrainPulse

To run BrainPulse run the following comand in a terminal window:
```bash
python3 -m brainpulse -i ../data/config.yml
```

with '*../data/config.yml* being the path for the input file you would like to process.

The command above will create the output files requested in the input file.


## Output files

Two kind of output files are available :

### Paraview

The results are stored in a *'.vtp* format in the *'outputdir/jobname/paraview/* folder.

Here is an example of the paraview visualisation of the results obtained when running the example *'WillisCircle.yml*.

![Example of the paraview visualisation of results](/docs/images/circle-of-willis.png)

We recommend to use the following filters : 
- CellDataToPointData;
- Tube with vary radius by scalar option.




### Path
The results along a path in the vascular network are stored into a *'.dat* file  in the <'outputdir/jobname/pathdata/> folder.



# Author
This tool has been created and implemented by:

- Alexandra Vallet [1] (author, developer, maintainer)

1. University of Oslo, Oslo, Norway



# Acknowledgments

- I would like to thanks Sylvie Lorthois, Kent Mardal and Miroslav Kuchta for the helpful discussions about the models used in this tool.


